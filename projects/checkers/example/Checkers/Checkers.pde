// -----------------------------------------------------------------------------
// Blair Watkinson
// Demo of Checkers Model
// Developed for CS101 to demonstrate MVC approach to design and development
// -----------------------------------------------------------------------------


// -----------------------------------------------------------------------------
// Constant Declarations
// -----------------------------------------------------------------------------
boolean DEBUG = false;


// -----------------------------------------------------------------------------
// Global variables
// -----------------------------------------------------------------------------
String input = "";
CheckersGameModel game = new CheckersGameModel();


// -----------------------------------------------------------------------------
// void setup()
// Initialize state variables and screen
// -----------------------------------------------------------------------------
void setup() {
  size(200, 200);  
  printGame();
}

// -----------------------------------------------------------------------------
// void draw()
// Update screen with input string
// -----------------------------------------------------------------------------
void draw() {
  background(255);
  textAlign(CENTER, CENTER);
  fill(0);
  text(input, width/2, height/2);
  textSize(20);
}

// -----------------------------------------------------------------------------
// void keyPressed()
// Capture key board input
// When 5 characters have been pressed followed by enter, send command to 
//   Checkers model
// Update input string for display to screen
// -----------------------------------------------------------------------------
void keyPressed() {
  if (key == '\n') {
    if (input.length() == 5) {
      if (game.move(input.substring(0, 2), input.substring(3))) {
        printGame();
      } else {
        println(input + " is an invalid move");
        printGame();
      }
    } else {
      println(input + " is an improperly formatted move");
      printGame();
    }
    input = "";
  } else if (key == '\b') {
    input = input.substring(0, input.length()-1);
  } else {
    input += key;
  }
}

// -----------------------------------------------------------------------------
// void printGame()
// Display current game status and player/winner
// -----------------------------------------------------------------------------
void printGame() {
  int winner = game.getWinner();
  if (winner == 0) {
    println("Player " + game.getPlayerTurn() + " turn:");
  } else {
    println("Player " + winner + " wins!");
  }
  print(game);
}